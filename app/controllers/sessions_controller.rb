class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user
      if user.authenticate(params[:session][:password])
        log_in user
        redirect_to user, notice: "Welcome to toy_app_demo"
      else
        flash.now[:error] = "Mật khẩu không đúng. Vui lòng nhập lại!"
        render 'new'
      end
    else
      flash.now[:error] = "Email mà bạn đã nhập không khớp với bất kỳ tài khoản nào trong hệ thống. Vui lòng thử lại hoặc tạo tài khoản mới bằng cách nhấn vào liên kết Sign up ở bên dưới. "
      render 'new'
    end
  end

  def destroy
    log_out
    redirect_to root_url
  end
end
